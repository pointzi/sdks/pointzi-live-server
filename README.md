# Pointzi Live (pointzi-tether)

App to debug and test Pointzi on Apps

## Install the dependencies
```bash
npm install
cd server 
npm install
```

### Start the server and app
```bash
 bash ./run.sh
```

### Lint the files
```bash
npm run lint
```

### Build the app for production (does not build the server)
```bash
quasar build
```

### for window 10
- open two terminals (one for server folder, one for root folder)
- 'npm i' for both terminals
- on server folder, do 'npm run start'
- on root folder, do 'npm i -g @quasar/cli' then  'quasar dev'