#!/usr/bin/env bash


cd server || exit

npm i

npm run start &
pid=$!

cd ..

npm i

quasar dev

trap "kill ${pid}; exit 1" INT
