
const express = require('express')
const socketio = require('socket.io')
const app = express()
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.get('/', (req, res) => {
  res.render('index')
})
const server = app.listen(process.env.PORT || 9303, () => {
  console.log('server is running')
})

// initialize socket for the server
const io = socketio(server)

io.on('connection', socket => {
  console.log('New user connected')

  // handle the new message event
  socket.on('STATE_BROADCAST', data => {
    console.log('State received')
    // console.log(data)

    io.sockets.emit('STATE_BROADCAST', { state: data })
  })
  // handle the new message event
  socket.on('NEW_LOGLINE', data => {
    console.log('Logline received')
    // console.log(data)

    io.sockets.emit('NEW_LOGLINE', { logline: data })
  })
  // handle the new message event
  socket.on('NEW_DEBUG_LOG', (level, message) => {
    // console.log('Debug log received')
    // console.log(level, message)

    io.sockets.emit('NEW_DEBUG_LOG', { level, message, timestamp: Date.now() })
  })
  // handle the new message event
  socket.on('SET_SYS_STATE', data => {
    console.log('State change request received')
    console.log(data)

    io.sockets.emit('SET_SYS_STATE', data)
  })
  // handle the new message event
  socket.on('APP_STATUS', data => {
    console.log('AppStatus received')
    console.log(data)

    io.sockets.emit('APP_STATUS', data)
  })
  // handle the new message event
  socket.on('FEEDS', data => {
    console.log('Feeds received')
    console.log(data)

    io.sockets.emit('FEEDS', data)
  })
  // handle the new message event
  socket.on('SET_STATE_FIELD', data => {
    console.log('State change request received')
    console.log(data)

    io.sockets.emit('SET_STATE_FIELD', data)
  })
})
