import VueSocketIO from 'vue-socket.io'

export default ({ app, router, Vue, store }) => {
  Vue.use(new VueSocketIO({
    debug: true,
    connection: 'http://localhost:9303',
    vuex: {
      store,
      actionPrefix: 'SOCKET_',
      mutationPrefix: 'SOCKET_'
    }
  }))
}
