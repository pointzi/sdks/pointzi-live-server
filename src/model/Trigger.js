export default class Trigger {
  constructor (feedID,
    campaignID,
    expiredID,
    tool,
    setup,
    view,
    actioned,
    json,
    trigger,
    triggerType,
    target,
    launcher,
    stepCount,
    campaignType,
    deleteConditionRepeat) {
    this.feedID = feedID
    this.campaignID = campaignID
    this.expiredID = expiredID
    this.tool = tool
    this.setup = setup
    this.view = view
    this.actioned = actioned
    this.json = json
    this.trigger = trigger
    this.triggerType = triggerType
    this.target = target
    this.launcher = launcher
    this.stepCount = stepCount
    this.campaignType = campaignType
    this.deleteConditionRepeat = deleteConditionRepeat
  }
}
