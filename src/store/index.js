import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  return new Vuex.Store({
    state: {
      payload: {
        device: {
          timeZone: '',
          carrier: '',
          appVersion: '',
          appVersionName: '',
          appName: '',
          displaySizePx: '',
          displaySizeDp: '',
          isGooglePlay: '',
          platformName: '',
          downloadSource: '',
          id: '',
          isConnected: ''
        },
        app: {
          key: 'capivara',
          activity: {
            NewsActivity: 'org.wikipedia.feed.news.NewsActivity',
            PageActivity: 'org.wikipedia.page.PageActivity',
            OnThisDayActivity: 'org.wikipedia.feed.onthisday.OnThisDayActivity',
            ConfigureActivity: 'org.wikipedia.feed.configure.ConfigureActivity',
            MainActivity: 'org.wikipedia.main.MainActivity',
            DescriptionEditSuccessActivity: 'org.wikipedia.descriptions.DescriptionEditSuccessActivity',
            DescriptionEditTutorialActivity: 'org.wikipedia.descriptions.DescriptionEditTutorialActivity',
            InitialOnboardingActivity: 'org.wikipedia.onboarding.InitialOnboardingActivity'
          }
        },
        user: {
          id: ''
        },
        install: {
          id: '88a5b13e-1694-4b1e-b4f5-031171ae90e6',
          timestamp: 1600249768212,
          state: true
        },
        session: {
          id: 20,
          timestamp: 1600249852318
        },
        current_activity: {
          name: 'MainActivity',
          friendly_name: 'org.wikipedia.main.MainActivity',
          timestamp: { MainActivity: '1600249852327' }
        },
        network: {
          loglines: {
            bundle_id: 110,
            bundle_limit: 45,
            disabled: [],
            priority: [8202, 8203, 20, 21, 22, 8103, 8104, 8112, 8050, 8051, 8997, 8998, 8999, 8200, 8201]
          }
        },
        tags: {
          filter: '(.*)',
          level: 'ENABLED'
        }
      },
      loglines: [],
      debugLogs: [],
      debugPaused: false
    },
    mutations: {
      'SOCKET_STATE_BROADCAST' (state, data) {
        state.payload = JSON.parse(data.state)
      },
      'SOCKET_NEW_LOGLINE' (state, { logline }) {
        if (state.loglines.length > 10) {
          state.loglines.splice(0, 1)
        }
        state.loglines.push(JSON.parse(logline))
      },
      'SOCKET_NEW_DEBUG_LOG' (state, log) {
        if (!state.debugPaused) {
          if (state.debugLogs.length > 1000) {
            state.debugLogs.splice(0, 1)
          }
          state.debugLogs.push(log)
        }
      },
      'CLEAR_DEBUG' (state) {
        state.debugLogs = []
      },
      'TOGGLE_DEBUG_PAUSE' (state) {
        state.debugPaused = !state.debugPaused
      }
    }
  })
}
